package com.vn.pufusoft.samenavigationdrawer;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.widget.FrameLayout;

public class AboutUsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout contentFrameLayout =  findViewById(R.id.activity_base_content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_about_us, contentFrameLayout);
        NavigationView navigationView = findViewById(R.id.activity_base_nav_view);
        navigationView.getMenu().getItem(1).setChecked(true);
    }
}
